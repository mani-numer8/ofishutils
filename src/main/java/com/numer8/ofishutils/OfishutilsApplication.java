package com.numer8.ofishutils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfishutilsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfishutilsApplication.class, args);
	}

}
