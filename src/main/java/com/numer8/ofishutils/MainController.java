package com.numer8.ofishutils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MainController {
	private static String baseUrl = "http://159.65.143.72/Projects/ofish/weather/icons/";
	@GetMapping("/generateToken")
	public @ResponseBody String generateTokenForDev() {
		StringBuffer response = new StringBuffer();
		try {
			String url = "https://numer8.maps.arcgis.com/sharing/rest/generateToken";
			URL obj = new URL(url);
			java.net.HttpURLConnection con = (java.net.HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			String urlParameters = "referer=https://numer8.maps.arcgis.com&username=Mani_Numer8&password=Starwars2008$&f=json";
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println(response.toString());
		}catch(Exception e) {
			System.out.println(e);
		}
		return response.toString();
	}
	
	@GetMapping("/getWeather")
	public @ResponseBody String getWeather(@RequestParam(value = "lat") float lat,@RequestParam(value = "lon") float lon
			, @RequestParam(value = "hrs") int hrs) {
		JSONObject response = new JSONObject();
		try {
			String key = "8030350d7ae24b59b57142756192112";
			String url = "http://api.worldweatheronline.com/premium/v1/marine.ashx?" + "q="+lat+","+lon+"&tide=yes&key="+key+"&format=json";
			String pattern = "yyyy-MM-dd";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			String currentDate = simpleDateFormat.format(new Date());
			int hr = (int)((Math.ceil(hrs/3)) * 300);
			JSONObject json = readJsonFromUrl(url);
			//response = json.toString();
			JSONArray dataArray = (json.getJSONObject("data")).getJSONArray("weather");
			JSONArray forecastForWeek = new JSONArray();
			JSONArray hourlyData = new JSONArray();
			for (int i = 0; i < dataArray.length(); i++) {
				JSONObject data = dataArray.getJSONObject(i);
				boolean hasTideData = false;
				if(data.get("tides")!=null) {
					if(data.getJSONArray("tides").length()>0) {
						if(data.getJSONArray("tides").getJSONObject(0).get("tide_data")!=null) {
							if(data.getJSONArray("tides").getJSONObject(0).getJSONArray("tide_data").length()>0) {
								hasTideData = true;
							}
						}
					}
				}
				JSONObject dailydata = new JSONObject();
				dailydata.put("Day", "Day"+(i + 1));
				dailydata.put("Temp", Integer.parseInt(String.valueOf(data.getJSONArray("hourly").getJSONObject(4).get("tempC"))));
				dailydata.put("CloudCover", Integer.parseInt(String.valueOf(data.getJSONArray("hourly").getJSONObject(4).get("cloudcover"))));
				dailydata.put("WindSpeed", Integer.parseInt(String.valueOf(data.getJSONArray("hourly").getJSONObject(4).get("windspeedKmph"))));
				dailydata.put("WindDirection", (String.valueOf(data.getJSONArray("hourly").getJSONObject(4).get("winddir16Point"))));
				dailydata.put("HeightOfWave", Float.parseFloat(String.valueOf(data.getJSONArray("hourly").getJSONObject(4).get("swellHeight_m"))));
				dailydata.put("WindSpeedIcon", getWindSpeedIcon(Integer.parseInt(String.valueOf(data.getJSONArray("hourly").getJSONObject(4).get("windspeedKmph")))));
				dailydata.put("WaveHeightIcon", getWaveHeightIcon(Float.parseFloat(String.valueOf(data.getJSONArray("hourly").getJSONObject(4).get("swellHeight_m")))));
				JSONArray tideData = new JSONArray();
				if(hasTideData) {
					int di = 0;
					JSONArray rawTideData = data.getJSONArray("tides").getJSONObject(0).getJSONArray("tide_data");
					for(int j = 0;j < rawTideData.length();j++) {
						JSONObject dailyTideData = new JSONObject();
						dailyTideData.put("TideTime", rawTideData.getJSONObject(j).getString("tideTime"));
						dailyTideData.put("TideHeight", rawTideData.getJSONObject(j).getString("tideHeight_mt"));
						dailyTideData.put("TideIcon", getTideIcon(rawTideData.getJSONObject(j).getString("tide_type")));
						dailyTideData.put("TideDateTime", rawTideData.getJSONObject(j).getString("tideDateTime"));
						dailyTideData.put("TideType", rawTideData.getJSONObject(j).getString("tide_type"));
						tideData.put(dailyTideData);
						di++;
					}
				}
				dailydata.put("Tide", tideData);
				String dailyDataIcon = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png";
				if(data.getJSONArray("hourly").getJSONObject(4).get("weatherIconUrl")!=null) {
					if(data.getJSONArray("hourly").getJSONObject(4).getJSONArray("weatherIconUrl").length()>0) {
						if(data.getJSONArray("hourly").getJSONObject(4).getJSONArray("weatherIconUrl").getJSONObject(0).getString("value")!=null) {
							dailyDataIcon = String.valueOf(data.getJSONArray("hourly").getJSONObject(4).getJSONArray("weatherIconUrl").getJSONObject(0).getString("value"));
						}
					}
				}
				dailydata.put("Icon", dailyDataIcon);
				forecastForWeek.put(dailydata);
				
				JSONArray rawHourlyData = data.getJSONArray("hourly");
				if(currentDate.equals(data.getString("date"))) {
					for(int j = 0;j < rawHourlyData.length();j++) {
						JSONObject hData = rawHourlyData.getJSONObject(j);
						if(hourlyData.length()<8) {
							if(hr==2400) {
								Date cDate = new Date();
								Date nextDay = new Date(cDate.getTime() + (1000 * 60 * 60 * 24));
								currentDate = simpleDateFormat.format(nextDay);
								hr=0;
							}else if(hr==hData.getInt("time")){
								JSONObject nhData = new JSONObject();
								nhData.put("Name", hrToHrStr(hr));
								nhData.put("Temp", hData.getInt("tempC"));
								nhData.put("CloudCover", hData.getInt("cloudcover"));
								nhData.put("WindSpeed", hData.getInt("windspeedKmph"));
								nhData.put("WindDirection", hData.getString("winddir16Point"));
								nhData.put("HeightOfWave", hData.getFloat("swellHeight_m"));
								nhData.put("WindSpeedIcon", getWindSpeedIcon(hData.getInt("windspeedKmph")));
								nhData.put("WaveHeightIcon", getWaveHeightIcon(hData.getFloat("swellHeight_m")));
								nhData.put("Tide", new JSONArray());
								String hourlyDataIcon = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png";
								if(hData.get("weatherIconUrl")!=null) {
									if(hData.getJSONArray("weatherIconUrl").length()>0) {
										if(hData.getJSONArray("weatherIconUrl").getJSONObject(0).getString("value")!=null) {
											hourlyDataIcon = String.valueOf(hData.getJSONArray("weatherIconUrl").getJSONObject(0).getString("value"));
										}
									}
								}
								nhData.put("Icon", hourlyDataIcon);
								hourlyData.put(nhData);
								hr+=300;
							}
						}
					}
				}
			}
			response.put("ForecastForWeek", forecastForWeek);
			response.put("Hourly", hourlyData);
		}catch(Exception e) {
			System.out.println(e);
		}
		return response.toString();
	}
	
	private static String hrToHrStr(int hr) {
		String hf_str = "";
		switch (hr) {
			case 0:
				hf_str = "12 AM";
				break;
			case 300:
				hf_str = "3 AM";
				break;
			case 600:
				hf_str = "6 AM";
				break;
			case 900:
				hf_str = "9 AM";
				break;
			case 1200:
				hf_str = "12 PM";
				break;
			case 1500:
				hf_str = "3 PM";
				break;
			case 1800:
				hf_str = "6 PM";
				break;
			case 2100:
				hf_str = "9 PM";
				break;
		}
		return hf_str;
	}
	
	private static String getTideIcon(String tide) {
		if(tide.equals("HIGH")) {
			return baseUrl + "tide_high.png";
		}else {
			return baseUrl + "tide_low.png";
		}
	}
	
	private static String getWaveHeightIcon(float wh) {
		if(wh>=0 && wh<0.25){
			return baseUrl+"wh_1.png";
		}else if(wh<0.5){
			return baseUrl+"wh_2.png";
		}else if(wh<0.75){
			return baseUrl+"wh_3.png";
		}else if(wh<1){
			return baseUrl+"wh_4.png";
		}else if(wh<1.25){
			return baseUrl+"wh_5.png";
		}else{
			return baseUrl+"wh_5.png";
		}
	}
	
	private static String getWindSpeedIcon(int ws) {
		if(ws >= 0 && ws <= 2){
			return baseUrl + "ws_1_2.png";
		}else if(ws <= 7){
			return baseUrl + "ws_3_7.png";
		}else if(ws <= 12){
			return baseUrl + "ws_3_7.png";
		}else if(ws <= 17){
			return baseUrl + "ws_13_17.png";
		}else if(ws <= 22){
			return baseUrl + "ws_18_22.png";
		}else if(ws <= 27){
			return baseUrl + "ws_23_27.png";
		}else if(ws <= 32){
			return baseUrl + "ws_28_32.png";
		}else if(ws <= 37){
			return baseUrl + "ws_33_37.png";
		}else if(ws <= 42){
			return baseUrl + "ws_38_42.png";
		}else if(ws <= 47){
			return baseUrl + "ws_43_47.png";
		}else if(ws <= 52){
			return baseUrl + "ws_48_52.png";
		}else if(ws <= 57){
			return baseUrl + "ws_53_57.png";
		}else if(ws <= 62){
			return baseUrl + "ws_58_62.png";
		}else{
			return baseUrl + "ws_58_62.png";
		}
	}
	
	private static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }

	  public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
	    InputStream is = new URL(url).openStream();
	    try {
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
	      String jsonText = readAll(rd);
	      JSONObject json = new JSONObject(jsonText);
	      return json;
	    } finally {
	      is.close();
	    }
	  }
}
